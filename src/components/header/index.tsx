import { Link } from "react-router-dom";
import { HeaderData } from "../../types/HeaderData";
import "./header.css";

declare interface HeaderProps {
  settings: HeaderData;
}

function Header(props: HeaderProps): JSX.Element {
  return (
    <header className="header">
      <Link to="/">
        <img src={props.settings.headerImage} />
      </Link>
      <h1>{props.settings.title}</h1>
      <Link to="/Profile">Welcome, {props.settings.userName}</Link>
    </header>
  );
}

export default Header;
