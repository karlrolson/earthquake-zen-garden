import { HeaderData } from "../../types/HeaderData";
import Header from "../header";
declare interface LayoutProps {
  children: React.ReactNode;
  header: HeaderData;
}

function Layout(props: LayoutProps): JSX.Element {
  return (
    <div>
      <Header settings={props.header} />
      {props.children}
    </div>
  );
}

export default Layout;
