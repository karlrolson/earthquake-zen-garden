export declare interface Site {
  title: string;
  heroImage: string;
  logoImage: string;
}
