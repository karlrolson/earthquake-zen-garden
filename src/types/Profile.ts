export declare interface Profile {
  avatarImage: string;
  bio: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}
