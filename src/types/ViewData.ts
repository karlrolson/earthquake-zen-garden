import { AppData } from "./AppData";
import { HeaderData } from "./HeaderData";

export declare interface ViewData {
  data: AppData;
  header: HeaderData;
}
