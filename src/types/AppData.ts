import { FeatureCollection } from "./FeatureCollection";
import { Profile } from "./Profile";
import { Site } from "./Site";

export declare interface AppData {
  data: FeatureCollection;
  profile: Profile;
  site: Site;
}
