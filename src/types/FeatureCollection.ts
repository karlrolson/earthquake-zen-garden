import { Feature } from "./Feature";

export declare interface FeatureCollection {
  bbox: number[];
  features: Feature[];
  metadata: {
    api: string;
    count: number;
    generated: number;
    status: number;
    title: string;
    url: string;
  };
  type: string;
}
