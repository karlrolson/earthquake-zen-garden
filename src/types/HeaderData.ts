export declare interface HeaderData {
  title: string;
  headerImage: string;
  userName: string;
}
