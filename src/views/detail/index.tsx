import Layout from "../../components/layout";
import { ViewData } from "../../types/ViewData";
import useQuery from "../../hooks/useQuery";
import localizeTimestamp from "../../utils/localizeTimestamp";
import "./detail.css";

function Detail(props: ViewData): JSX.Element {
  const query = useQuery();
  const id = query.get("id");
  const data = props.data.data.features.find((quake) => quake.id == id) ?? null;
  const properties = data?.properties;
  return (
    <Layout header={props.header}>
      {data && properties && (
        <div className="detailList">
          <h2>{properties.title}</h2>
          <div className="detailEntry">
            <div>Title</div>
            <div>{properties.title}</div>
          </div>
          <div className="detailEntry">
            <div>Magnitude</div>
            <div>{properties.mag}</div>
          </div>
          <div className="detailEntry">
            <div>Time</div>
            <div>{localizeTimestamp(properties.time)}</div>
          </div>
          <div className="detailEntry">
            <div>Status</div>
            <div>{properties.status}</div>
          </div>
          <div className="detailEntry">
            <div>Tsunami</div>
            <div>{properties.tsunami}</div>
          </div>
          <div className="detailEntry">
            <div>Type</div>
            <div>{data.type}</div>
          </div>
        </div>
      )}
      {(!data || !properties) && <h2>Quake Data Not Found</h2>}
    </Layout>
  );
}

export default Detail;
