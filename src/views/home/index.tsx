import { useState } from "react";
import { Link } from "react-router-dom";
import Layout from "../../components/layout";
import { Feature } from "../../types/Feature";
import { ViewData } from "../../types/ViewData";
import localizeTimestamp from "../../utils/localizeTimestamp";
import "./home.css";

function Home(props: ViewData): JSX.Element {
  const [sortType, setSortType] = useState("mag");
  const [sortDirection, setSortDirection] = useState(true);

  // didn't want to use any, but it was expedient here
  const currentSortFunction = (a: any, b: any) => {
    let aVal = a[sortType];
    let bVal = b[sortType];

    if (sortDirection === true) {
      // descending
      return aVal > bVal ? -1 : 1;
    } else {
      // ascending
      return bVal > aVal ? -1 : 1;
    }
  };

  const tableData = props.data.data.features
    .map((quake) => {
      // premap just to get all the formatting in for the sort on title
      // though how you would sort on title here is it's own fun question
      // IE: sort on location name, distance, unformatted string, etc.
      // also to make time sorting based on the unformatted time
      const formattedTime = localizeTimestamp(quake.properties.time);
      const title = quake.properties.title.split(" - ")[1];
      return {
        id: quake.id,
        formattedTime,
        mag: quake.properties.mag,
        time: quake.properties.time,
        title,
      };
    })
    .sort(currentSortFunction)
    .map((quake) => {
      return (
        <tr key={quake.id}>
          <td className="homeTableLeft">
            <Link to={`/Detail?id=${quake.id}`} className="homeTableLink">
              {quake.title}
            </Link>
          </td>
          <td>{quake.mag}</td>
          <td>{quake.formattedTime}</td>
        </tr>
      );
    });

  const updateSort = (newSortType: string) => {
    if (newSortType === sortType) {
      setSortDirection(!sortDirection);
    } else {
      setSortType(newSortType);
    }
  };

  return (
    <Layout header={props.header}>
      <div className="homeContainer">
        <h2>{props.data.data.metadata.title}</h2>
        <table className="homeTable">
          <thead>
            <tr>
              <th onClick={() => updateSort("title")}>Title</th>
              <th onClick={() => updateSort("mag")}>Magnitude</th>
              <th onClick={() => updateSort("time")}>Time</th>
            </tr>
          </thead>
          <tbody>{tableData}</tbody>
        </table>
      </div>
    </Layout>
  );
}

export default Home;
