import Layout from "../../components/layout";
import { ViewData } from "../../types/ViewData";
import "./profile.css";

function Profile(props: ViewData): JSX.Element {
  const profile = props.data.profile;
  const altText = `Profile Image of ${profile.firstName}`;
  return (
    <Layout header={props.header}>
      <div className="profileContainer">
        <h3>Profile</h3>
        <div className="profileContents">
          <div className="profileImage">
            <img src={profile.avatarImage} alt={altText} title={altText} />
          </div>
          <div className="profileData">
            <div className="profileEntry">
              <div>First Name</div>
              <div>{profile.firstName}</div>
            </div>
            <div className="profileEntry">
              <div>Last Name</div>
              <div>{profile.lastName}</div>
            </div>
            <div className="profileEntry">
              <div>Phone</div>
              <div>{profile.phone}</div>
            </div>
            <div className="profileEntry">
              <div>Email</div>
              <div>{profile.email}</div>
            </div>
            <div className="profileEntry">
              <div>Bio</div>
              <div>{profile.bio}</div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default Profile;
