import React, { useState } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./index.css";
import data from "../data/data.json";
import Home from "./views/home";
import Profile from "./views/profile";
import { AppData } from "./types/AppData";
import { HeaderData } from "./types/HeaderData";
import Detail from "./views/detail";

function Main(): JSX.Element {
  // yes, this an unpleasant way to put this data into the website.
  // A useEffect against an API endpoint would be more realistic.
  const [appData] = useState(data as AppData);
  const header: HeaderData = {
    headerImage: appData.site.logoImage,
    title: appData.site.title,
    userName: appData.profile.firstName,
  };
  return (
    <React.StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home data={appData} header={header} />} />
          <Route
            path="/Detail"
            element={<Detail data={appData} header={header} />}
          />
          <Route
            path="/Profile"
            element={<Profile data={appData} header={header} />}
          />
        </Routes>
      </BrowserRouter>
    </React.StrictMode>
  );
}
const root = document.getElementById("root");

ReactDOM.render(<Main />, root);
