// to be honest, I'd normally consider importing something to do this, but this seemed suitable for the task
function localizeTimestamp(timestamp: number) {
  return new Date(timestamp).toLocaleString("en-US");
}

export default localizeTimestamp;
