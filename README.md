# Earthquake Zen Garden

A quake data dashboard.

Built on Vite + ReactTS, as it was convenient, familiar and needs suiting.

## Getting started

Install:

- `npm install`

Run: (do install first)

- `npm run start` or `npm run dev`

## License

All rights reserved.

## Project Status

Completed, will be deleted ASAP.
